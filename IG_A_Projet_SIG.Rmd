---
title: "Information géographique"
subtitle: "Projet SIG : problématique et consignes"
author: "M1, Université de Paris"
date: "Hugues Pecout (FR CIST),  Ronan Ysebaert (UMS RIATE)"
output: 
  ioslides_presentation:
    widescreen: true
    css: styles.css
    logo: figures/logo.gif
    incremental: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

<script>
    $(document).ready(function() {
      $('slide:not(.title-slide, .backdrop, .segue)').append('<footer label=\"Projet SIG : problématique et consignes\"></footer>');    
    })
</script>


# Informations générales

## Le projet SIG... 

<br>

> - <span style="color:#9c2005">Est un travail **à réaliser en binôme**</span>  

> - <span style="color:#9c2005">Comptera pour **2/3 de la note finale**</span>  

> - <span style="color:#9c2005">Rendu en **format papier le 7 janvier 2021, bureau 707**</span>  

</br>

Vous aurez l'occasion de **travailler et d'avancer sur votre dossier durant de nombreuses séances. Profitez-en !**


# Problématique

## Mise en situation...

<span style="font-size:18pt;">Vous êtes **membre d'un bureau d'étude chargé de réaliser une pré-étude pour l'implantation d'un nouveau équipement** public dans une des communautés d’agglomération de la Métropole du Grand Paris.</span>


<span style="font-size:18pt;color:#9c2005;">**L'objectif de cette pré-étude est de proposer une zone d'implantation** pour ce nouvel équipement, en vous basant sur **plusieurs critères d'implantation** que vous aurez préalablement déterminé et justifié.</span>


<span style="font-size:18pt;">**Vous devez livrer un document qui résume l'ensemble de vos choix, de votre démarche et de vos traitements** qui vous ont permis de déterminer une (ou plusieurs) zone.s d'implantation optimale.s</span>


## 

<span style="font-size:18pt;">**1) Choisissez une des communautés d'agglomérations** de la MGP (**Paris exclu**)</span>

```{r, echo = FALSE, out.width = "600px", fig.align='center'}
knitr::include_graphics('figures/carte_projet.png')
```

## 

<span style="font-size:19pt;">**2) Choisissez un type d'équipement public** parmi ces cinq propositions :</span>


<br>

<P style="color:#9c2005;font-size:18pt;">

> - <span style="color:#9c2005;">**Médiathèque**</span> 

> - <span style="color:#9c2005;">**Conservatoire de musique**</span> 

> - <span style="color:#9c2005;">**Salle de cinéma**</span> 

> - <span style="color:#9c2005;">**Mission locale pour l’emploi**</span> 
</p>


> - <span style="color:#9c2005;line-height:1;font-size:18pt;">**Maison de retraite**</span> 


# Document à livrer

## Organisation du contenu

> - <span style="font-size:14pt;">**1) Courte introduction présentant votre espace d'étude et l'équipement** que vous souhaitez implanter. Expliquer **pourquoi l'implantation de ce nouvel équipement vous semble pertinent**.</span> <span style="color:#9c2005;font-size:14pt;line-height:1;">(**1 point**)</span>

> - <span style="font-size:14pt;">**2) Présentation des critères d'implantation (4 minimum)** choisis pour determiner la localisation optimale. **Justifiez la pertinence de vos critères**. N'hésitez pas à vous appuyer sur de la documentation ! </span> <span style="color:#9c2005;font-size:14pt;">(**3 points**)</span>

> - <span style="font-size:14pt;">**3) Restitution des étapes de traitement SIG** qui vous ont permis de determiner la ou les zone.s optimale.s. **Concluez cette partie par une carte de synthèse résumant l'ensemble de la démarche et la (ou les) zone.s d'implantation proposée.s**  </span> <span style="color:#9c2005;font-size:14pt;">(**5 points**)</span>

> - <span style="font-size:14pt;">**4) Illustration graphique de la chaîne de traitement réalisée**. </span> <span style="color:#9c2005;font-size:14pt;">(**2 points**)</span>

> - <span style="font-size:14pt;">**5) Conclusion. Proposez des critères d'implémentation supplémentaires** qui pourraient améliorer la qualité votre étude</span> <span style="color:#9c2005;font-size:14pt;">(**2 points**)</span>  


## Cartographie obligatoires

Votre rapport devra **obligatoirement contenir 3 cartes thématiques, accompagnées de commentaires descriptifs** :

> - <span style="font-size:16pt;">**1)** Une **carte représentant des données quantitatives absolues**.<span style="color:#9c2005;font-size:14pt;line-height:1;">(**2 points**)</span>    
*Ex : Nombre d'habitants par commune*</span>

> - <span style="font-size:16pt;">**2)** Une **carte représentant des données quantitatives relatives**.<span style="color:#9c2005;font-size:14pt;line-height:1;">(**2 points**)</span>    
*Ex : Part des moins de 25 ans par commune*</span> 

> - <span style="font-size:16pt;">**3)** Une **carte représentant des données qualitatives**.<span style="color:#9c2005;font-size:14pt;line-height:1;">(**2 points**)</span>    
*Ex : Carte de localisation et présentation de votre espace d'étude.*</span> 



## Grille de notation


<br>


> - <span style="font-size:19pt;">**13 points** pour le **la réflexion et le travail SIG** *> QGIS*</span> 
> - <span style="font-size:19pt;">**6 points** pour les **cartes thématiques** *> Magrit*</span> 
> - <span style="font-size:19pt;">**1 point** pour le **soin**</span> 

<br>

<span style="color:#9c2005;font-size:19pt;">**Et 1 point en moins par jour de retard !**</span> 

```{r, echo = FALSE, out.width = "120px", fig.align='left'}
knitr::include_graphics('figures/smiley.png')
```


# Pour vous guider...

## Un exemple type

<span style="font-size:14pt;">Pour vous guider dans votre projet et vous inititier à QGIS et MAGRIT, **tous les tutoriels et démonstrations seront réalisés à partir du même exemple** :</span><span style="color:#9c2005;font-size:14pt;">**L'implantation d'une médiathèque** au sein de la CA **Est-Ensemble**</span>

<div class="columns-2">
<div class="centered">
<img src="figures/seance5BIS.png" width="500" />
</div>
<div class="centered">
<br>  
<br>  
<span style="font-size:16pt;">**vous ne pouvez pas choisir     
cet exemple** pour votre projet !</span>
</div>
</div>

## Séance 1 : Introduction



<span style="font-size:16pt;color:#9c2005;">**Cours**</span>    

> -  <span style="font-size:14pt;">**Information géographique** : Définition, sources, méthodes et outils</span>        
> -  <span style="font-size:14pt;">**Projet SIG : problématique**, consignes et données mises à diposition</span>      

</br>

<span style="font-size:16pt;color:#9c2005;">**Atelier pratique**</span>   

> -  <span style="font-size:14pt;">**Constitution des binômes**</span>    
> -  <span style="font-size:14pt;">Choix d'une CA et d'un type d'équipement</span>         
> -  <span style="font-size:14pt;">**Réflexion collective** sur les critères d'implantation</span>     




## Séance 2 : SIG (1)

<div class="columns-2">

<span style="font-size:16pt;color:#9c2005;">**Cours**</span>   

> -  <span style="font-size:14pt;">Information géographique : **Coordonées géographiques** & **projection cartographique**</span> 
> -  <span style="font-size:14pt;">Information géographique : **Introduction aux SIG**</span>
> -  <span style="font-size:14pt;">Projet SIG : **Premiers pas avec QGIS**</span> 



<span style="font-size:16pt;color:#9c2005;">**Atelier QGIS**</span>

> -  <span style="font-size:14pt;">**Présentation de QGIS**</span> 
> -  <span style="font-size:14pt;">**Importer de l'information géographique**</span> 
> -  <span style="font-size:14pt;">Gestion des **projection cartographique**</span>   
> -  <span style="font-size:14pt;">Gestion des **styles graphiques**</span>   

<div class="centered">

<img src="figures/seance2.PNG" width="300" />
<br>
<span style="font-size:12pt;">*Import de l'espace d'étude (polygones) et des équipements existants (points)*</span> 

</div>
</div>

## Séance 3 : SIG (2)

<br>

<div class="columns-2">

<span style="font-size:16pt;color:#9c2005;">**Cours-Atelier QGIS**</span>   

> -  <span style="font-size:14pt;">**Import de données statistiques**</span>  
> -  <span style="font-size:14pt;">**Jointure** de données</span>   
> -  <span style="font-size:14pt;">**Sélection** d'entitités **par attribut** ou par **localisation**</span>      
> -  <span style="font-size:14pt;">Enregistrement d'une sélection</span>   
> -  <span style="font-size:14pt;">Découverte des **géotraitements**</span>   

<div class="centered">

<img src="figures/seance3.png" width="330" />
<br>
<span style="font-size:12pt;">*Sélection des entités caractérisées par une valeur supérieure à... pour tel indicateur *</span> 

</div>
</div> 
 
 
## Séance 4 : SIG (3)

<div class="columns-2">

<span style="font-size:16pt;color:#9c2005;">**Cours & Pratique**</span>

> -  <span style="font-size:14pt;">**Formalisation graphique d''une chaîne de traitement**</span>

<div class="centered">

<img src="figures/seance5.png" width="300" />

</div>
</div> 

<div class="columns-2">

<br>

<span style="font-size:16pt;color:#9c2005;">**Atelier QGIS**</span>    

> -  <span style="font-size:14pt;">**Composeur d'impression** (mise en page de carte)</span>   

<div class="centered">


<img src="figures/seance5BIS.png" width="310" />

</div>
</div> 


## Séance 5 : Cartographie thématique (1)

<div class="columns-2">

<span style="font-size:16pt;color:#9c2005;">**Cours**</span>  

> -  <span style="font-size:14pt;">Information géographique : **Introduction à la cartographie thématique**</span>

> -  <span style="font-size:14pt;">Information géographique : **Spécificités des données quantitatives absolues**</span>

<br>


<span style="font-size:16pt;color:#9c2005;">**Atelier Magrit**</span>    

> -  <span style="font-size:14pt;">**Présentation** du logiciel **Magrit**</span>   
> -  <span style="font-size:14pt;">**Cartographier des données quantitatives absolues**</span>  
> -  <span style="font-size:14pt;">**Mettre en page sa carte**</span>   

<div class="centered">

<img src="figures/seance7.png" width="500" />

</div>
</div> 


## Séance 6 : Cartographie thématique (2)

<div class="columns-2">

<span style="font-size:16pt;color:#9c2005;">**Cours**</span>

<span style="font-size:14pt;">**Spécificités des données quantitatives relatives (discrétisation) et qualitatives**</span>

<br>

<span style="font-size:16pt;color:#9c2005;">**Atelier Magrit**</span>   

<span style="font-size:14pt;">**Cartographier des données quantitatives relatives et les données qualitatives**</span>   

<div class="centered">


<img src="figures/seance8.png" width="500" />

</div>
</div> 


## Séance 7

<div class="centered">
<span style="font-size:17pt;color:#9c2005;">**Séance de travail collective sur votre projet**</span>    

<img src="figures/collectif.png" width="600" />
</div>


# Des questions ?
