Source : IGN / Geofla, 2018
URL : http://professionnels.ign.fr/geofla

D�partements : Contient les d�partements de la m�tropole du Grand Paris. 

Communes : Contient les communes de la m�tropole du Grand Paris.

IRIS : Contient les IRIS de la m�tropole du Grand Paris. Ceux-ci sont rendus disponibles par �tablissement public territoiral. 

Note : Au fond de carte initial a �t� associ� l'�tablissement public territorial d'appartenance de chaque objet g�ographique (UMS RIATE). 
Les espaces d'�tude utiles pour le projet ont �t� extraits des fonds g�ographiques de r�f�rence. 